$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 2000
    });
    $('#contacto').on('show.bs.modal', function (e) {
        console.log('el modal se está mostrando');

        $('#contactoBtn').removeClass('btn-outline-succes');
        $('#contactoBtn').addClass('btn-primary');
        $('#contactoBtn').prop('disable', true);

    });
    $('#contacto').on('shown.bs.modal', function (e) {
        console.log('el modal se mostro');
    });
    $('#contacto').on('hide.bs.modal', function (e) {
        console.log('el modal se oculta');
    });
    $('#contacto').on('hidden.bs.modal', function (e) {
        console.log('el modal se ocultó');
        $('#contactoBtn').prop('disable', false);
    });
});